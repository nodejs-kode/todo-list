import express from "express";
import prisma from "../db/index.mjs";

const productController = express.Router()

import { getAllProducts, createProduct, updateProduct, deleteProduct } from "./service.mjs";

productController.get("/", async (req, res, next) => {
    try {
        const products = await getAllProducts()
        res.status(200).send(products)
    } catch (error) {
        next(error)
    }
})
.post("/", async (req,res, next) => {
    try {
        if (
            !(req.body.name &&
                req.body.price
            )
        ){
            return res.status(400).send("Some field is missing")
        }

        const product = await createProduct(req)

        res.status(200).send({
            message: 'create product success!',
            data: product,
        })
    } catch (error) {
        next(error)
    }
})
.put("/:id", async (req,res, next) => {
    try {
        let productId = req.params.id
        if (
            !(req.body.name &&
                req.body.price
            )
        ){
            return res.status(400).send("Some field is missing")
        }
        const update = await updateProduct(req, productId)
        res.status(200).send({
            message: "success updated",
            data: update 
        })
    } catch (error) {
        next(error)
    }
})
.delete("/:id", async (req,res, next) => {
    try {
        const productId = parseInt(req.params.id)
        const deleted = await deleteProduct(productId)
        res.status(200).send({ message : 'delete success' })
    } catch (error) {
        next(error)
    }
})

export default productController