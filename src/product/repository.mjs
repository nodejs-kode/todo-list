import prisma from "../db/index.mjs";

const allProduct = async () => {
    const products = await prisma.product.findMany()
    return products
}

const create = async (req) => {
    const create = await prisma.$transaction(async(prisma) => {
        await prisma.product.create({
            data:{
                name: req.body.name,
                price: req.body.price
            }
        })
    })

    return create
}

const update = async (req,productId) => {
    const update = await prisma.$transaction( async (prisma) => {
        await prisma.product.update({
            where: {
                id:parseInt(productId)
            },
            data:{
                name : req.body.name,
                price : req.body.price,
            }
        })
    }) 
    return update
}

const deleted = async(productId) => {
    const deleted = await prisma.$transaction( async (prisma) => {
        await prisma.product.delete({
            where:{ id: productId }
        })
    }) 
    return deleted
}

export {
    allProduct,
    create,
    update,
    deleted
}