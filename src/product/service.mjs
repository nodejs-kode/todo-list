import prisma from "../db/index.mjs";

import { allProduct, create, deleted, update } from "./repository.mjs"; 

const getAllProducts = async () => {
    const data = await allProduct()
    return data
}

const createProduct = async (req) => {
    const data = await create(req)
    return data
}

const updateProduct = async (req, productId) => {
    const data = await update(req, productId)
    return data
}

const deleteProduct = async (productId) => {
    const data = await deleted(productId)
    return data
}

export {
    getAllProducts,
    createProduct,
    updateProduct,
    deleteProduct
}