import express from "express";
import 'dotenv/config'
import cors from "cors";

import winston from "winston";

// Controller
import productController from "./product/controller.mjs";

const app = express()
const port = process.env.PORT


const logger = winston.createLogger({
    level: "error",
    format: winston.format.printf(info => {
        return `${new Date()} | Error : ${info.level.toUpperCase()} : ${info.message}`
    }),
    transports: [
      new winston.transports.File({ filename: 'error.log' })
    ]
});

const errorHandlerMiddleware = (err, req, res, next) => {
    // Handle the error
    logger.error(`${Date.now} error : ${err}`)
    res.status(500).json({ error: err });
};

app.use(cors())
app.use(express.json())

app.use('/product', productController)

app.use(errorHandlerMiddleware)

app.listen(port, () => {
    console.info(`Running in port : ${port}`)
})