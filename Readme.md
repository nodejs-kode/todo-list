###################
REST API TODO-LIST
###################

*********
Framework
*********
- `ExpressJS`

*********
ORM
*********
- `Prisma`

*********
Database
*********
- `PostgreSQL`

*********
LOGGER
*********
- `Winston`
